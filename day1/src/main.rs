use itertools::Itertools;
use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();

    buf_reader.read_to_string(&mut contents)?;
    Ok(contents)
}

// fn find_2020(input: Vec<i32>, n: usize) -> Option<Vec<i32>> {
fn find_2020(input: &[i32], n: usize) -> Option<Vec<i32>> {
    input
        .iter()
        .cloned()
        .combinations(n)
        .find(|x| x.iter().sum::<i32>() == 2020)
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: Vec<i32> = match read_input(&args[1]) {
        Ok(text) => text
            .trim()
            .split('\n')
            .map(|x| x.parse::<i32>().unwrap())
            .collect(),
        Err(_) => panic!("Er is iets fout gegaan."),
    };

    // part 1
    if let Some(v) = find_2020(&input, 2) {
        println!("{:?}", v.iter().product::<i32>())
    }

    // part 2
    if let Some(v) = find_2020(&input, 3) {
        println!("{:?}", v.iter().product::<i32>())
    }

    Ok(())
}
