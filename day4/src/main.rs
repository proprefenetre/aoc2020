use std::collections::HashMap;
use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();

    buf_reader.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}

fn valid_passport<'a>(passport: Vec<&'a str>) -> Option<HashMap<&'a str, &'a str>> {
    let fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

    let vals: HashMap<&str, &str> = passport
        .iter()
        .map(|x| x.split(':').collect::<Vec<&str>>())
        .map(|v| (v[0], v[1]))
        .collect();

    if fields.iter().all(|&x| vals.contains_key(x)) {
        Some(vals)
    } else {
        None
    }
}

fn check_passport(passport: &HashMap<&str, &str>) -> bool {
    // byr (Birth Year) - four digits; at least 1920 and at most 2002.
    // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    // hgt (Height) - a number followed by either cm or in:
    //    - If cm, the number must be at least 150 and at most 193.
    //    - If in, the number must be at least 59 and at most 76.
    // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    // pid (Passport ID) - a nine-digit number, including leading zeroes.
    // cid (Country ID) - ignored, missing or not.
    passport.into_iter().all(|(key, val)| {
        match *key {
            "byr" => {
                return val
                    .parse::<i32>()
                    .ok()
                    .filter(|x| (1920..=2002).contains(x))
                    .is_some()
            }
            "iyr" => {
                return val
                    .parse::<i32>()
                    .ok()
                    .filter(|x| (2010..=2020).contains(x))
                    .is_some()
            }
            "eyr" => {
                return val
                    .parse::<i32>()
                    .ok()
                    .filter(|x| (2020..=2030).contains(x))
                    .is_some()
            }
            "hgt" if val.ends_with("cm") => {
                return val
                .split(|x: char| x.is_alphabetic())
                .next()
                .and_then(|x| x.parse::<i32>().ok())
                .filter(|x| (150..=193).contains(x))
                .is_some()
        }
        "hgt" if val.ends_with("in") => {
            return val
                .split(|x: char| x.is_alphabetic())
                .next()
                .and_then(|x| x.parse::<i32>().ok())
                .filter(|x| (59..=76).contains(x))
                .is_some()
        }
        "hcl" if val.starts_with("#") => {
            return val.chars().skip(1).all(|x| x.is_ascii_hexdigit())
        }
            "ecl" => return matches!(*val, "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth"),
            "pid" => return val.len() == 9,
            "cid" => return true,
            _ => return false,
        };
    })
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = read_input(&args[1])?;

    // part 1
    let passports: Vec<HashMap<&str, &str>> = input
        .split("\n\n")
        .map(|x| x.lines().flat_map(|x| x.split(" ")).collect())
        .filter_map(|x| valid_passport(x))
        .collect();

    println!("{}", passports.len());

    // part 2
    let valids: usize = passports
        .iter()
        .filter(|&x| check_passport(x))
        .count();

    println!("{}", valids);

    Ok(())
}
