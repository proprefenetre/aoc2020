use std::collections::HashMap;
use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();

    buf_reader.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}

fn check_bag(rules: &HashMap<&str, Vec<Vec<&str>>>, bag: &str) -> bool {
    let shiny = "shiny gold bag";
    if bag == shiny {
        return true;
    }
    rules
        .get(bag)
        .iter()
        .map(|x| x.iter().any(|y| check_bag(rules, y[1])))
        .next()
        .unwrap_or(false)
}

fn count_bags(rules: &HashMap<&str, Vec<Vec<&str>>>, bag: &str) -> i32 {
    rules
        .get(bag)
        .unwrap()
        .iter()
        .map(|x| x[0].parse::<i32>().unwrap() * count_bags(&rules, x[1]))
        .sum::<i32>()
        + 1
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = read_input(&args[1])?;

    // part 1
    let rules: HashMap<&str, Vec<_>> = input
        .lines()
        .map(|x| -> Vec<&str> {
            x.split("contain")
                .map(|y| y.trim_end_matches(|c: char| c == ' ' || c == 's' || c == '.'))
                .collect::<Vec<_>>()
        })
        .map(|v| -> (&str, Vec<Vec<&str>>) {
            (
                v[0],
                v[1].split(", ")
                    .map(|x| -> &str {
                        x.trim_start_matches(' ').trim_end_matches("s")
                    })
                    .filter(|&x| x != "no other bag")
                    .map(|x| x.splitn(2, " ").collect::<Vec<_>>())
                    .collect::<Vec<_>>(),
            )
        })
        .collect();

    // part 1
    println!(
        "{}",
        rules.keys().fold(0, |acc, k| {
            if check_bag(&rules, k) {
                acc + 1
            } else {
                acc
            }
        }) - 1
    ); // -1 voor de Shiny Gold Bag-regel

    // part 2 - How many individual bags are required inside your single shiny gold bag?
    println!("{}", count_bags(&rules, "shiny gold bag") - 1);

    Ok(())
}
