use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    Ok(contents)
}

fn count_valid_passwords(passwords: &[&str]) -> Result<i32, &'static str> {
    let mut valid = 0;
    for pw in passwords {
        if let [r, l, w] = pw.split(' ').collect::<Vec<_>>().as_slice() {
            if let [first, second] = r
                .split('-')
                .map(|x| x.parse::<i32>())
                .collect::<Vec<_>>()
                .as_slice()
            {
                let first = *first.as_ref().expect("not a number");
                let second = *second.as_ref().expect("not a number");
                let l = l.strip_suffix(":").expect("I don't know, man");
                let count: i32 = w.to_string().matches(l).count() as i32;
                if !((count as i32) < first || (count as i32) > second) {
                    valid += 1;
                }
            }
        }
    }
    Ok(valid)
}

fn match_position(password: &str, pos: i32, letter: char) -> bool {
    password.chars().nth(pos as usize).expect(&format!("Nothing at position {}: {}", pos, password)) == letter
}

fn count_valid_passwords_2(passwords: &[&str]) -> Result<i32, &'static str> {
    let mut valid: i32 = 0;
    for pw in passwords {
        if let [r, l, w] = pw.split(' ').collect::<Vec<_>>().as_slice() {
            if let [first, second] = r
                .split('-')
                .map(|x| x.parse::<i32>())
                .collect::<Vec<_>>()
                .as_slice()
            {
                let first = *first.as_ref().expect("not a number") as i32;
                let second = *second.as_ref().expect("not a number") as i32;
                let letter: char = l.strip_suffix(":").expect("Not a letter").chars().nth(0).expect("I don't know, man");
                
                let matches = vec![match_position(w, first - 1, letter), match_position(w, second - 1, letter)];
                if !(matches.iter().all(|&x| x == true) || matches.iter().all(|&x| x == false)) {
                    valid += 1;
                }
            }
        }
    }
    Ok(valid)
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = match read_input(&args[1]) {
        Ok(text) => text.trim().to_string(),
        Err(_) => panic!("Er is iets fout gegaan."),
    };

    let passwords: Vec<&str> = input.lines().collect();

    println!("{:?}", count_valid_passwords_2(&passwords));
    Ok(())
}
