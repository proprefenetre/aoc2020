use itertools::Itertools;
use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();

    buf_reader.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}

fn count_common(grp: Vec<&str>) -> i32 {
    let mut count: i32 = 0;

    for c in grp.concat().chars().unique() {
        if grp.iter().all(|g| g.contains(c)) {
            count += 1;
        }
    }

    count
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = read_input(&args[1])?;

    // part 1
    println!(
        "{:?}",
        input
            .split("\n\n")
            .map(|x| x.replace("\n", ""))
            .map(|x| x.chars().unique().count())
            .sum::<usize>()
    );

    //part 2
    println!("{:?}", input
             .split("\n\n")
             .map(|x| x.lines().collect())
             .map(|x| count_common(x))
             .sum::<i32>()
    );

    Ok(())
}
