use std::io::prelude::*;
use std::{env, fs, io};

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();

    buf_reader.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}

fn halves(inp: Vec<u32>) -> (Vec<u32>, Vec<u32>) {
    let mid: usize = inp.len() / 2;
    (inp[..mid].to_vec(), inp[mid..].to_vec())
}

fn partition<'a>(inp: Vec<u32>, side: char) -> Option<Vec<u32>> {
    let (f, b) = halves(inp);
    match side {
        'F' | 'L' => Some(f),
        'B' | 'R' => Some(b),
        _ => None,
    }
}

fn seat(boarding_pass: &str) -> u32 {
    let chars: Vec<char> = boarding_pass.chars().collect();

    let mut rows: Vec<u32> = (0..128).collect();
    let mut cols: Vec<u32> = (0..8).collect();

    for l in chars.iter() {
        match l {
            'F' | 'B' => {
                rows = partition(rows, *l).unwrap();
            }
            'L' | 'R' => {
                cols = partition(cols, *l).unwrap();
            }
            _ => (),
        }
    }

    rows[0] * 8 + cols[0]
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = read_input(&args[1])?;

    let mut seats = input.lines().map(|x| seat(x)).collect::<Vec<_>>();

    // part 1
    if let Some(m) = seats.iter().max() {
        println!("{}", m);
    }

    // smart solution:
    let bin_seats = input
        .lines()
        .map(|x| x
             .chars()
             .map(|y| match y {
                 'F' | 'L' => '0',
                 'B' | 'R' => '1',
                 _ => ' ',
             })
             .collect::<String>())
        .map(|x| isize::from_str_radix(&x, 2).unwrap())
        .collect::<Vec<_>>();

    // part 2
    seats.sort();

    for (idx, s) in seats.iter().enumerate() {
        if seats[idx + 1] != s + 1 {
            println!("{}", s + 1);
            break;
        }
    }

    // smarter solution
    for s in bin_seats.iter() {
        if !bin_seats.contains(&(s + 1)) && bin_seats.contains(&(s + 2)) {
            println!("{}", s + 1);
        }
    }

    Ok(())
}
