use std::error;
use std::io::prelude::*;
use std::{env, fs, io};

// use itertools::Itertools;

fn read_input(path: &str) -> io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = io::BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}

fn count_trees(map: &[&str], right: usize, down: usize) -> Result<i32, Box<dyn error::Error>> {
    let mut n_trees: i32 = 0;
    let mut x: usize = 0;

    for row in map.into_iter().step_by(down) {
        if row.chars().nth(x % row.len()).expect("Off the map!") == '#' {
            n_trees += 1;
        }
        x += right;
    }
    Ok(n_trees)
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let input: String = read_input(&args[1])?;

    let map: Vec<&str> = input.lines().collect();
    let slopes: Vec<(i32, i32)> = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    // part 1
    println!("{}", count_trees(&map, 3, 1).expect("Can't slope this."));

    // part 2
    println!(
        "{}",
        slopes
            .iter()
            .map(|&(x, y)| count_trees(&map, x as usize, y as usize).expect("Can't product this.") as i64)
            .product::<i64>()
    );
    Ok(())
}
